import { Component, OnInit } from '@angular/core';
import {IndexserviceService} from '../indexservice.service';
// import {BookingserviceService} from '../bookingservice.service';
// import {WorkflowService} from '../workflow/workflow.service';
import { ActivatedRoute, Router}  from '@angular/router';
import { BookingParams } from '../dto/tour';
import { Accommodation, Room } from '../dto/accomodation';
import { Hotel } from '../dto/accomodation';
import { Stage } from '../dto/accomodation';
import * as $ from 'jquery';
import { WorkflowService } from '../workflow/workflow.service';
import { STEPS }              from '../workflow/workflow.model';
declare var require: any
import Utils from '../utils'
@Component({
  selector: 'app-accommodations',
  templateUrl: './accommodations.component.html',
  styleUrls: ['./accommodations.component.css']
})
export class AccommodationsComponent implements OnInit {
  parentMessage = "accommodations"
  private sub: any;
  private bookingParams;
 //bookingParams:BookingParams;
 private accommodation:Accommodation;
 error:string;
 loading:boolean;
 response:string;
  constructor(private _indexService: IndexserviceService,private workflowService: WorkflowService,private route: ActivatedRoute,private router:Router) { }

  ngOnInit() {
    this.sub = this.route.queryParams.subscribe(params => {
      this.bookingParams = params; // (+) converts string 'id' to a number
    
   });
    //this.bookingParams=this._bookingService.getbookingParams();    
    this.accoInfo();
  }
  selectHotel(form: any,hotelCode,tourcode,hotelName,accType)
  {
    
    if (form.valid) {
    this.workflowService.validateStep(STEPS.accommodations);
    let params=new BookingParams();
    params.reservationId=this.accommodation.reservationId;
    params.tourSearchId=this.accommodation.tourSearchId;
    params.tourDepartureId=this.accommodation.tourDepartureId;
    params.hotelCode=hotelCode;
    params.hotelName=hotelName;
    params.tourCode=this.accommodation.tourCode;
    params.accType=accType;
    params.tourTransport= this.bookingParams.tourTransport;
    params.NPC= this.bookingParams.NPC;
    params.deeplink= this.bookingParams.deeplink;
    params.tourCode= this.bookingParams.tourCode;
    params.tourDepDate=this.bookingParams.tourDepDate;
    params.depPointName=this.bookingParams.depPointName;
    params.fAirport=this.bookingParams.fAirport;
    params.fStation=this.bookingParams.fStation;
    params.fAdults=this.bookingParams.fAdults
    params.fChildren= this.bookingParams.fChildren
    params.fInfants= this.bookingParams.fInfants
    params.tourName=this.bookingParams.tourName
    
    //this._bookingService.setbookingParams(params);
    // Navigate to the rooms page
    this.router.navigate(['/rooms'],{ queryParams:  params });
    }
  }
  accoInfo()
  {
    this.loading = true;
    
    this._indexService.accoInfo(this.bookingParams).subscribe(
       data => {
        this.loading = false;
        this.response=data;
        /*to convert xml text to javascript object*/
        let convert = require('xml-js');
        let result  = convert.xml2js(data, {compact: true, spaces: 4});
        if(!result.root.error){   
        
        let accommodation= new Accommodation();
        /*  Itinerary details*/
        if(result.root.bookvars.depPointName)
        {
        accommodation.depPointName= result.root.bookvars.depPointName._cdata;
        }
        accommodation.tourCode= result.root.bookvars.tourCode._cdata;
        accommodation.tourName= result.root.bookvars.tourName._cdata;
        accommodation.fAdults= result.root.bookvars.fAdults._cdata;
        accommodation.fChildren= result.root.bookvars.fChildren._cdata;
        accommodation.tourTransport =result.root.bookvars.tourTransport._cdata;
        
        accommodation.tourDepDate= Utils.format(result.root.bookvars.tourDepDate._cdata);

        accommodation.reservationId= result.root.bookvars.reservationId._text;
        accommodation.tourSearchId= result.root.bookvars.tourSearchId._text;
        accommodation.tourDepartureId= result.root.bookvars.tourDepartureId._text;
         /*  Flight details*/
         accommodation.stages=[];
         var stages =$.makeArray( result.root.xResponse.itinerary.stages.stage );
         stages.forEach(element => {
           if(element.type._text == 'T')
           {
             let stage= new Stage();
             stage.arrives=element.arrives._cdata;
             stage.departs=element.departs._cdata;
             stage.deptime=element.deptime._text;
             stage.arrtime=element.arrtime._text;
             stage.description=element.description._cdata;
             stage.date=Utils.format(element.date._text);
             accommodation.stages.push(stage)
           }
         });
           /*  Accommodation choices*/
         accommodation.hotels=[];
         var hotels =$.makeArray( result.root.xResponse.hotels.hotel );
         
         hotels.forEach(element => {
          let hotel= new Hotel();
          hotel.name=element.name._cdata;
          hotel.accTypeText=element.accTypeText._text;       
          hotel.boardBasisDescr=element.boardBasisDescr._cdata;
          hotel.pic=element.pic._cdata;
          hotel.code=element.code._text;
          hotel.tourcode=element.tourcode._text;
          hotel.rooms=[];
          var rooms =$.makeArray( element.rooms.room );
          rooms.forEach(element => {
            let room= new Room();
            room.descr=element.descr._cdata;   
            room.wasPrice=element.wasPrice._text; 
            room.rate=element.rate._text; 
            room.unitNumber = element.unitNumber._cdata;
            hotel.rooms.push(room);
            
          });
          accommodation.hotels.push(hotel)
         });
        
        
        
        this.accommodation= accommodation;
        }
        else{
          this.error=result.root.error._text;
          console.error(result.root.error._text)
        }
        },
       err => console.error(err),
       () => console.log('done loading accommodations')
     );
   }

}
