import { BrowserModule } from '@angular/platform-browser';
import { NgModule,  CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';  // replaces previous Http service
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HolidaysComponent } from './holidays/holidays.component';
import { CoachComponent } from './coach/coach.component';
import { OceanCruisesComponent } from './ocean-cruises/ocean-cruises.component';
import { RiverCruisesComponent } from './river-cruises/river-cruises.component';
import { EventsComponent } from './events/events.component';
import { EntertainmentComponent } from './entertainment/entertainment.component';
import { HomeComponent } from './home/home.component';
import { IndexserviceService } from './indexservice.service';
import { ProductComponent } from './product/product.component';
import { AccommodationsComponent } from './accommodations/accommodations.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavbarComponent } from './navbar/navbar.component';
import { RoomsComponent } from './rooms/rooms.component';
import { ExtrasComponent } from './extras/extras.component';
import { PersonalComponent } from './personal/personal.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { CompleteComponent } from './complete/complete.component';
import { WorkflowGuard }        from './workflow/workflow-guard.service';
import { WorkflowService }      from './workflow/workflow.service';
import { ReevooReviewsComponent } from './reevoo-reviews/reevoo-reviews.component';
import { CheckPasswordDirective } from './check-password.directive';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { MyDatePickerModule } from 'mydatepicker';

const appRoutes: Routes = [
  { path: 'holidays', component: HolidaysComponent },
  { path: 'events', component: EventsComponent },
  { path: 'river-cruises', component: RiverCruisesComponent },
  { path: 'ocean-cruises', component: OceanCruisesComponent },
  { path: 'events', component: EventsComponent },
  { path: 'entertainment', component: EntertainmentComponent },
  { path: 'coach', component: CoachComponent },
  { path: '', component: HomeComponent },
  { path: 'tour', component: ProductComponent },  
  { path: 'accommodations', component: AccommodationsComponent },
  { path: 'rooms', component: RoomsComponent  },
  { path: 'extras', component: ExtrasComponent  },
  { path: 'personal', component: PersonalComponent  },
  { path: 'confirm', component: ConfirmComponent },
  { path: 'complete', component: CompleteComponent  }
 
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HolidaysComponent,
    CoachComponent,
    OceanCruisesComponent,
    RiverCruisesComponent,
    EventsComponent,
    EntertainmentComponent,
    HomeComponent,
    ProductComponent,
    AccommodationsComponent,
    NavbarComponent,
    RoomsComponent,
    ExtrasComponent,
    PersonalComponent,
    ConfirmComponent,
    CompleteComponent,
    ReevooReviewsComponent,
     CheckPasswordDirective,
     DatepickerComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,                               // <========== Add this line!
    ReactiveFormsModule ,   
    MyDatePickerModule ,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [IndexserviceService,WorkflowGuard,{ provide: WorkflowService, useClass: WorkflowService }],
  bootstrap: [AppComponent]
})
export class AppModule { }
