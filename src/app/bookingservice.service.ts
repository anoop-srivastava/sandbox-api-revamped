import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BookingserviceService {

  constructor() { }
  private bookingParams;

  setbookingParams(data){
    this.bookingParams = data;
  }

  getbookingParams(){
    let temp = this.bookingParams;
    this.clearData();
    return temp;
  }

  clearData(){
    this.bookingParams = undefined;
  }
}
