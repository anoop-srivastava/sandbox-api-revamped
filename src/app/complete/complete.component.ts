import { Component, OnInit } from '@angular/core';
import { IndexserviceService } from '../indexservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery';
declare var require: any
import { Complete, CardProcessing, ConfirmedData, Footnotes, PostalAddress, CostBreakdown, CostingArea, CostingSummary } from '../dto/complete';

@Component({
  selector: 'app-complete',
  templateUrl: './complete.component.html',
  styleUrls: ['./complete.component.css']
})
export class CompleteComponent implements OnInit {
  parentMessage = "complete"
  private sub: any;
  bookingParams;
  error: string;
  private compete: Complete;
  loading: boolean;
  response: string;
  complete: Complete
  constructor(private _indexService: IndexserviceService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.sub = this.route.queryParams.subscribe(params => {
      
      this.bookingParams = params; // (+) converts string 'id' to a number

    });
    this.confirmInfo();
  }
  confirmInfo() {
    this.loading = true;
    this._indexService.completeInfo(this.bookingParams).subscribe(

      data => {
        //var data='<?xml version="1.0" encoding="UTF-8"?> <root> <aboutMe> <versionRef><![CDATA[2.0]]></versionRef> <revisionDate><![CDATA[01 Jan 2019]]></revisionDate> <dataServer><![CDATA[xThankYouServer]]></dataServer> <datestamp><![CDATA[12/03/2019 15:02:14]]></datestamp> <location><![CDATA[https://api.newmarket.travel/xBooking/Svr2]]></location> </aboutMe> <NPC><![CDATA[NMP]]></NPC> <deeplink><![CDATA[NMP17318]]></deeplink> <custCode><![CDATA[NMP]]></custCode> <trackerID/> <xResponse bookStage="ThankYou"> <cardProcessing> <cardProcessErrCde><![CDATA[0]]></cardProcessErrCde> <cardProcessErrMsg><![CDATA[Payment Status: Completed]]></cardProcessErrMsg> <WorldPayPaymentStatus><![CDATA[Completed]]></WorldPayPaymentStatus> <WorldPayOrderKey><![CDATA[Payment In Full]]></WorldPayOrderKey> </cardProcessing> <confirmedData> <leadName><![CDATA[Mr TEST TEST]]></leadName> <postalAddress> <CCardAddress1><![CDATA[TEST]]></CCardAddress1> <CCardAddress2><![CDATA[TEST]]></CCardAddress2> <CCardCity><![CDATA[TEST]]></CCardCity> <CCardPostCode><![CDATA[TEST]]></CCardPostCode> <CCardCountry><![CDATA[United Kingdom]]></CCardCountry> </postalAddress> <mainEmail><![CDATA[TEST@gmail.com]]></mainEmail> <reservationId><![CDATA[1209247]]></reservationId> <bookingRef><![CDATA[S047515]]></bookingRef> <tourcode/> <holidayName><![CDATA[Belfast and the Titanic Experience ]]></holidayName> <adults><![CDATA[2]]></adults> <children><![CDATA[0]]></children> <infants><![CDATA[0]]></infants> <departureDate><![CDATA[22 Sep 19]]></departureDate> <duration><![CDATA[3 nights]]></duration> <childAgeNote/> <holidayDetails/> <paxDetails> <passenger><![CDATA[Mr TEST TEST]]></passenger> <passenger><![CDATA[Mrs TEST TEST]]></passenger> </paxDetails> <paxAccomDetails> <paxAccom><![CDATA[Mr TEST TEST - ]]></paxAccom> <paxAccom><![CDATA[Mrs TEST TEST - ]]></paxAccom> </paxAccomDetails> <costBreakdown> <costingArea> <item><![CDATA[Tour package (2 x Adults)]]></item> <quantity><![CDATA[1]]></quantity> <price pcur="GBP"><![CDATA[459.00]]></price> <total pcur="GBP"><![CDATA[918.00]]></total> </costingArea> <costingArea> <item><![CDATA[Glens of Antrim &amp; Giant\'s Causeway Excursion]]></item> <quantity><![CDATA[1]]></quantity> <price pcur="GBP"><![CDATA[29.00]]></price> <total pcur="GBP"><![CDATA[29.00]]></total> </costingArea> <costingSummary> <grandTotal pcur="GBP">867.00</grandTotal> <amountPaid pcur=""><![CDATA[867.00]]></amountPaid> <balance pcur="GBP"><![CDATA[0.00]]></balance> <balanceDueDate><![CDATA[28 Jul 19]]></balanceDueDate> </costingSummary> </costBreakdown> </confirmedData> <footnotes> <final><![CDATA[This booking has now been made, and an email confirmation will be sent to you shortly. Should you need to contact us before this arrives, please call 0330 160 7707 during office hours. (Standard landline charges apply.)]]> </final> <paynote><![CDATA[Any balance still outstanding must be paid by the due date shown on this page. A final invoice will not be sent.<br/>Final travel documentation will be sent to you approximately one week prior to your departure.<br/><b>Thank you for booking with Newmarket!</b>]]></paynote> <address><![CDATA[Any future correspondence will be mailed to this address.]]></address> </footnotes> </xResponse> </root>'
        this.loading = false;
        this.response = data;
        /*to convert xml text to javascript object*/
        let convert = require('xml-js');
        let result = convert.xml2js(data, { compact: true, spaces: 4 });

        if (!result.root.error) {
          let complete = new Complete()

          let cardProcessing = new CardProcessing();
          cardProcessing.cardProcessErrCde = result.root.xResponse.cardProcessing.cardProcessErrCde._cdata
          cardProcessing.worldPayPaymentStatus = result.root.xResponse.cardProcessing.WorldPayPaymentStatus._cdata
          cardProcessing.cardProcessErrMsg = result.root.xResponse.cardProcessing.cardProcessErrMsg._cdata
          complete.cardProcessing = cardProcessing

          let footnotes = new Footnotes()
          footnotes.address = result.root.xResponse.footnotes.address._cdata
          footnotes.final = result.root.xResponse.footnotes.final._cdata
          footnotes.paynote = result.root.xResponse.footnotes.paynote._cdata
          complete.footnotes = footnotes

          let confirmedData = new ConfirmedData()

          confirmedData.leadName = result.root.xResponse.confirmedData.leadName._cdata
          confirmedData.adults = result.root.xResponse.confirmedData.adults._cdata
          confirmedData.bookingRef = result.root.xResponse.confirmedData.bookingRef._cdata
          confirmedData.childAgeNote = result.root.xResponse.confirmedData.childAgeNote._cdata
          confirmedData.children = result.root.xResponse.confirmedData.children._cdata
          confirmedData.mainEmail = result.root.xResponse.confirmedData.mainEmail._cdata
          confirmedData.reservationId = result.root.xResponse.confirmedData.reservationId._cdata
          confirmedData.holidayName = result.root.xResponse.confirmedData.holidayName._cdata
          confirmedData.infants = result.root.xResponse.confirmedData.infants._cdata
          confirmedData.departureDate = result.root.xResponse.confirmedData.departureDate._cdata
          confirmedData.duration = result.root.xResponse.confirmedData.duration._cdata
          let postalAddress = new PostalAddress();
          postalAddress.CCardAddress1 = result.root.xResponse.confirmedData.postalAddress.CCardAddress1._cdata
          postalAddress.CCardAddress2 = result.root.xResponse.confirmedData.postalAddress.CCardAddress2._cdata
          postalAddress.CCardCity = result.root.xResponse.confirmedData.postalAddress.CCardCity._cdata
          postalAddress.CCardCountry = result.root.xResponse.confirmedData.postalAddress.CCardCountry._cdata
          postalAddress.CCardPostCode = result.root.xResponse.confirmedData.postalAddress.CCardPostCode._cdata
          confirmedData.postalAddress = postalAddress

          let paxs = $.makeArray(result.root.xResponse.confirmedData.paxAccomDetails.paxAccom);
          confirmedData.paxAccomDetails = []
          paxs.forEach(element => {
            confirmedData.paxAccomDetails.push(element._cdata)
          });

          let costBreakdown = new CostBreakdown()
          costBreakdown.costingAreas = []
          let costings = $.makeArray(result.root.xResponse.confirmedData.costBreakdown.costingArea)
          costings.forEach(element => {
            let costingArea = new CostingArea()
            costingArea.item = element.item._cdata
            costingArea.price = element.price._cdata
            costingArea.quantity = element.quantity._cdata
            costingArea.total = element.total._cdata
            costBreakdown.costingAreas.push(costingArea)
          });
          let costingSummary = new CostingSummary();
          costingSummary.amountPaid=result.root.xResponse.confirmedData.costBreakdown.costingSummary.amountPaid._cdata
          costingSummary.balance=result.root.xResponse.confirmedData.costBreakdown.costingSummary.balance._cdata
          costingSummary.balanceDueDate=result.root.xResponse.confirmedData.costBreakdown.costingSummary.balanceDueDate._cdata
          costingSummary.grandTotal=result.root.xResponse.confirmedData.costBreakdown.costingSummary.grandTotal._text

          costBreakdown.costingSummary = costingSummary
          confirmedData.costBreakdown = costBreakdown
          complete.confirmedData = confirmedData

          this.complete = complete
         
        }

        else {
          this.error = result.root.error._text;
          console.error(result.root.error._text)
        }
      },
      err => console.error(err),
      () => console.log('done loading confirm')
    );
  }
}

