import { Component, OnInit } from '@angular/core';
import { IndexserviceService } from '../indexservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery';
declare var require: any
declare var BootstrapDialog: any
import Utils from '../utils'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { Confirm,Contact,CostDetails,CostSummary,PaymentOptions,Passenger, PayCard} from '../dto/confirm';
import { environment } from '../../environments/environment';
@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent implements OnInit {
  parentMessage = "confirm"
  private sub: any;
  bookingParams;
  error: string;
  private confirm: Confirm;
  opt:string;
  radioSelected:string;
  loading:boolean;
  response:string;
  constructor(private _indexService: IndexserviceService, private route: ActivatedRoute, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.sub = this.route.queryParams.subscribe(params => {
      this.bookingParams = params; // (+) converts string 'id' to a number

    });
    this.loading=false;
this.opt="full"

    this.confirmInfo();
    
  }
  submitPay(f: NgForm)
  {
    if (f.valid) {
      
      let params: any = {}
      
      params.reservationId = this.bookingParams.reservationId;
      params.tourSearchId = this.bookingParams.tourSearchId;
      params.tourDepartureId = this.bookingParams.tourDepartureId;
      params.hotelCode = this.bookingParams.hotelCode;
      params.hotelName = this.bookingParams.hotelName;
      params.tourCode = this.bookingParams.tourCode;
      params.accType = this.bookingParams.accType;
      params.tourTransport = this.bookingParams.tourTransport;
      params.NPC = this.bookingParams.NPC;
      params.deeplink = this.bookingParams.deeplink;
      params.tourCode = this.bookingParams.tourCode;
      params.tourDepDate = this.bookingParams.tourDepDate;
      params.depPointName = this.bookingParams.depPointName;
      params.fAirport = this.bookingParams.fAirport;
      params.fStation = this.bookingParams.fStation;
      params.fAdults = this.bookingParams.fAdults
      params.fChildren = this.bookingParams.fChildren
      params.fInfants = this.bookingParams.fInfants
      params.tourName = this.bookingParams.tourName
      params.TermsSel =  f.value["terms"]==true?"Y":"N";
      params.CCardId =  f.value["paymentOption"];
      params.CCardAmount = f.value["CCardAmount"];
      params.CCardCurrencyCode="GBP";
      
      let inputThankYou="?reservationId="+params.reservationId + "&tourSearchId=" +params.tourSearchId + "&tourDepartureId=" + params.tourDepartureId ;
      let port= window.location.port ? ":" + window.location.port :"";
      let retUrl=window.location.protocol + "//" +window.location.hostname + port;
      params.returnURL =encodeURIComponent(retUrl + environment.returlUrl +inputThankYou);
      
      this._indexService.postToPaymentGateway(params).subscribe(
        data => {
          /*to convert xml text to javascript object*/
        let convert = require('xml-js');
        let result = convert.xml2js(data, { compact: true, spaces: 4 });      
        window.location.href =result.root.xResponse.paymentGateway.paymentURL._cdata;
     
        },
        err => console.error(err),
        () => console.log('done loading ')
      );
      
    }
  }
  confirmInfo() {
    this.loading=true;
    this._indexService.confirmInfo(this.bookingParams).subscribe(

      data => {
        this.loading=false;
        this.response=data;
        /*to convert xml text to javascript object*/
        let convert = require('xml-js');
        let result = convert.xml2js(data, { compact: true, spaces: 4 });
      
        if (!result.root.error) {
        let confirm= new Confirm();
        confirm.tourTransport = result.root.bookvars.tourTransport._cdata;
        confirm.hotelName = result.root.bookvars.hotelName._cdata;
        confirm.accType = result.root.bookvars.accType._cdata;
        confirm.tourCode = result.root.bookvars.tourCode._cdata;
        if (result.root.bookvars.depPointName)
        confirm.depPointName = result.root.bookvars.depPointName._cdata;
        confirm.tourDepDate = Utils.format(result.root.bookvars.tourDepDate._cdata);
        confirm.deeplink = result.root.bookvars.deeplink._cdata;
        confirm.hotelCode = result.root.bookvars.hotelCode._cdata;

        confirm.fAdults = result.root.bookvars.fAdults._cdata;
        confirm.fChildren = result.root.bookvars.fChildren._cdata;
        confirm.tourName = result.root.bookvars.tourName._cdata;
        confirm.paxDetails=[];
        let paxs = $.makeArray(result.root.xResponse.confirmation.paxDetails.pax);
        paxs.forEach(element => {
          let passenger= new Passenger();
          passenger.paxName=element._cdata;
          passenger.paxType=element._attributes.paxType
          confirm.paxDetails.push(passenger);
        });
        let contact= new Contact();
        contact.leadPaxEmail=result.root.xResponse.confirmation.contactDetails.leadPaxEmail._cdata;
        contact.leadPaxPhone=result.root.xResponse.confirmation.contactDetails.leadPaxPhone._cdata;
        contact.CCardAddresses1=result.root.xResponse.confirmation.contactDetails.leadPaxPostalAddress.CCardAddresses1._text;
        contact.CCardAddresses2=result.root.xResponse.confirmation.contactDetails.leadPaxPostalAddress.CCardAddresses2._text;
        contact.CCardCities=result.root.xResponse.confirmation.contactDetails.leadPaxPostalAddress.CCardCities._text;
        contact.CCardCountry=result.root.xResponse.confirmation.contactDetails.leadPaxPostalAddress.CCardCountry._text;
        contact.CCardPostCodes=result.root.xResponse.confirmation.contactDetails.leadPaxPostalAddress.CCardPostCodes._text;
        confirm.contactDetails=contact;
        let costSummary= new CostSummary();
        costSummary.balancedue=result.root.xResponse.confirmation.costSummary.balancedue._text;
        costSummary.depositrequired=result.root.xResponse.confirmation.costSummary.depositrequired._text;
        costSummary.totalcost=result.root.xResponse.confirmation.costSummary.totalcost._text;
        confirm.costSummary=costSummary;

        confirm.costDetails=[];
        let costDetails = $.makeArray(result.root.xResponse.confirmation.costDetails.item);
        costDetails.forEach(element => {
          let costDetail= new CostDetails();
          costDetail.cost= element.cost._cdata;
          costDetail.descr= element.descr._cdata;
          confirm.costDetails.push(costDetail)
        });
        let paymentOptions= new PaymentOptions();
        paymentOptions.deposit=result.root.xResponse.confirmation.paymentOptions.deposit._text;
        paymentOptions.payfull=result.root.xResponse.confirmation.paymentOptions.payfull._text;
        let payCards = $.makeArray(result.root.xResponse.confirmation.paymentOptions.payCards.payCard);
        paymentOptions.payCards=[];
        payCards.forEach(element => {
          let payCard= new PayCard();
          payCard.id = element._attributes.id
          payCard.type = element._attributes.type
          payCard.value = element._cdata;
          paymentOptions.payCards.push(payCard)
        });
        confirm.paymentOptions=paymentOptions
        this.radioSelected=paymentOptions.payCards[0].id;
        this.confirm = confirm
        
        
        }
        
        else {
          this.error = result.root.error._text;
          console.error(result.root.error._text)
        }
      },
      err => console.error(err),
      () => console.log('done loading confirm')
    );
  }

}
