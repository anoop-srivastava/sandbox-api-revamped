export class Accommodation {
    tourName: string;
    fAdults: string;
    fChildren:string;
    depPointName: string;
    tourPickUp: string;
    tourDepDate:string;
    stages:Stage[];
    hotels:Hotel[];
    reservationId:string;
    tourSearchId:string;
    tourDepartureId:string;
    tourCode:string;
    tourTransport:string;
    constructor() { }
} 
export class Hotel {
    code: string;
    accTypeText: string;
    tourcode:string;
    name: string;
    rating: string;
    rooms:Room[];
    boardBasisDescr: string;
    pic:string;   
   
    constructor() { }
}

export class Room {
    descr: string;
    unitNumber: string;
    wasPrice:string;
    rate: string;   
    
    constructor() { }
} 
export class Stage {
    deptime: string;
    arrtime: string;
    departs:string;
    arrives:string;
    date:string;
    description: string;
    constructor() { }
} 