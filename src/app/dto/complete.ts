export class Complete {
    cardProcessing:CardProcessing;
    confirmedData:ConfirmedData
    footnotes:Footnotes
    constructor() { }
} 
export class CardProcessing {
    worldPayPaymentStatus:string
    cardProcessErrCde:string
    cardProcessErrMsg:string
    constructor() { }
} 
export class ConfirmedData {
   leadName:string
   adults:string
   bookingRef:string
   childAgeNote:string
   children:string
   postalAddress:PostalAddress
   mainEmail:string
   reservationId:string
   holidayName:string
   infants:string
   departureDate:string
   duration:string
   paxDetails:string[]
   paxAccomDetails:string[]
   costBreakdown:CostBreakdown
    constructor() { }
} 
export class Footnotes {
    address:string
    final:string
    paynote:string
    constructor() { }
} 

export class PostalAddress {
    CCardAddress1:string
    CCardAddress2:string
    CCardCity:string
    CCardPostCode:string
    CCardCountry:string
   
    constructor() { }
} 

export class CostBreakdown {
    costingAreas:CostingArea[]
    costingSummary:CostingSummary
   
    constructor() { }
} 
export class CostingArea {
    item:string
    quantity:string
    price:string
    total:string
   
   
    constructor() { }
} 

export class CostingSummary {
    grandTotal:string
    amountPaid:string
    balance:string
    balanceDueDate:string
   
   
    constructor() { }
} 