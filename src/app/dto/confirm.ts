

export class Confirm {
    
    tourTransport: string;
    hotelName:string;
    accType: string;
    tourCode: string;
    tourNts:string;
    deeplink: string;
    hotelCode:string;
    bcRooms: string;
    fAdults: string;
    fChildren:string;
    tourName:string;
    depPointName:string;
    tourDepDate:string;
    paxDetails:Passenger[];
    contactDetails:Contact;
    costDetails:CostDetails[];
    costSummary:CostSummary;
    paymentOptions:PaymentOptions;
    constructor() { }
} 

export class Passenger
{
    paxType:string;
    paxName:string;

    constructor() { } 
}

export class Contact
{
    CCardAddresses1:string;
    CCardAddresses2:string;
    CCardCities:string;
    CCardPostCodes:string;
    CCardCountry:string;
    leadPaxEmail:string;
    leadPaxPhone:string;
    leadPaxPhoneEve:string;
    constructor() { } 
}
export class CostSummary
{
    
    totalcost:string;
    depositrequired:string;
    balancedue:string
    constructor() { } 
}
export class CostDetails
{
    descr:string;
    cost:string;    
    constructor() { } 
} 



export class PaymentOptions
{
    payfull:string;
    deposit:string;
    payCards:PayCard[]
    constructor() { } 
}

export class PayCard
{
    id:string;
    type:string;
    value:string;
   
    constructor() { } 
}

