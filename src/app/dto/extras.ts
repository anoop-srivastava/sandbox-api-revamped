export class Extras {
    
    tourTransport: string;
    hotelName:string;
    accType: string;
    tourCode: string;
    tourNts:string;
    deeplink: string;
    hotelCode:string;
    bcRooms: string;
    fAdults: string;
    fChildren:string;
    tourName:string;
    depPointName:string;
    tourDepDate:string;
    blocks:Block[]
    constructor() { }
} 

export class Block {
    header:string;
    message:string;
    body:string;
    optionals:OptionalItem[]
    blockMin:string
    blockMax:string
}

export class OptionalItem {
    text: string;
    title:string;
    minQty: string;
    maxQty: string;
    bookingDate:string;
    descr:string;
    rate:string;
    xBookingKey:string;
    priceBasis:string;
    range:string[];
    tktAvail:string= "T";
}
export class SelectedExtra {
    qty:string;
    name:string;
    maxQty:string;
    minQty:string;
    bookingKey:string;
    blockName:string
    blockMax:string;
    blockMin:string;
}
export class ValidationMessage
{
    valid:boolean;
    msg:string;

}