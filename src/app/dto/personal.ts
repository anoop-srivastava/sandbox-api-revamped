export class Personal {
    tourTransport: string;
    hotelName:string;
    accType: string;
    tourCode: string;
    tourNts:string;
    deeplink: string;
    hotelCode:string;
    bcRooms: string;
    fAdults: string;
    fChildren:string;
    tourName:string;
    depPointName:string;
    tourDepDate:string;
    paxDetails:PaxInfo;
    paxNote:string
    marketingPreferences:MarketingPreferences[]
    constructor() { }
} 

export class PaxInfo
{
    paxnamesNote: string;
    paxCount: Number[];
    roomCount: string;
    paxTitles:string[];
    passengers:Passenger[];
    constructor() { } 
}
export class MarketingPreferences
{
    id: string;
    value: string;
    index:string;

    constructor() { } 
}

export class Passenger
{
    paxId: string;
    paxType: string
    rooms:Room[]
extras:Extra[]
    constructor() { } 
}

export class Room
{
    roomId: string;
    roomSelection: string
    minOcc: string
    maxOcc: string
    constructor() { } 
}

export class Extra
{
    minQty: string;
    maxQty: string
    group:string;
    options:Option[]

    constructor() { } 
}

export class Option
{
    responseId:string;
    text:string;

}

