 

export class Product {
    productname: string;
    availDates: string;
    dlc: string;
    image:string
    
    constructor() { }
} 

export class Genre {
    genrename: string;
    products:Array<Product> 
    
    constructor() { }
} 