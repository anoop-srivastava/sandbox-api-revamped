export class Room {
    uid: string;
    tourTransport: string;
    hotelName:string;
    accType: string;
    tourCode: string;
    tourNts:string;
    deeplink: string;
    hotelCode:string;
    bcRooms: string;
    fAdults: string;
    fChildren:string;
    tourName:string;
    depPointName:string;
    tourDepDate:string;
    availability:RoomingData[]
    constructor() { }
} 

export class RoomingData {
    minQty: string;
    maxQty: string;
    code:string;
    descr:string;
    rate:string;
    wasPrice:string;
    unitNumber:string;
    maxOcc:string;
    range:string[];
    minOcc:string;
    constructor() { }
} 

export class SelectedAccoUnit {
    qty: string;
    name: string;
    code:string;
    maxOcc:string;
    minOcc:string;
    constructor() { }
} 