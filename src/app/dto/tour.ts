
export class Tour {
    transportType: string;
    segment: string;
    genre: string;
    prodcat: string;
    holtype: string;
    prodName: string;
    prodWebName: string;
    deeplink: string;
    dateStamp: string;
    tourCode: string;
    bookOnLine: string;
    duration: string;
    period: string;
    fromPrice: string;
    fromPriceDate: string;
    intro: string;
    wpi: string;
    wpiTitle: string;
    tourIntro: string;
    tourIntroTitle: string;
    ItinTitle: string[];
    ItinBody: string[];
    furtherInfoTitle: string[];
    furtherInfoBody: string[];
    images:Image[];
    tourData:TourData[];
    npc: string;
    code: string;
    productCode: string;
    genreCode:string;
    introTitles:string[];
    introBodies:string[];
    constructor() { }
} 
export class Image {
    url: string;
    type:string;
    slot:string
    constructor() { }
}
export class TourData {
    name: string;
    code: string;
    dates:string[];
    
    constructor() { }
} 

export class BookingParams {
    NPC: string;
    deeplink: string;
    tourCode: string;
    tourDepDate: string;
    fAdults: string;
    fChildren: string;
    fInfants: string;
    depPointName: string;
    fAirport: string;
    fStation: string;
    tourTransport: string;
    touronly: string;
    agentCode: string;
    agentPassword: string;
    agentReference: string;
    agentContactEmail: string;
    tourName: string;
    reservationId: string;
    tourSearchId: string;
    tourDepartureId: string;
    hotelCode: string;
    hotelName: string;
    accType: string;
    constructor() {};
   

    
    
    
    
} 
