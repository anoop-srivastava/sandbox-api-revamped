import { Component, OnInit } from '@angular/core';
import { IndexserviceService } from '../indexservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery';
import { Extras, OptionalItem, Block, SelectedExtra, ValidationMessage } from '../dto/extras';
import Utils from '../utils'
import { element } from '@angular/core/src/render3';
declare var require: any
declare var BootstrapDialog: any
@Component({
  selector: 'app-extras',
  templateUrl: './extras.component.html',
  styleUrls: ['./extras.component.css']
})
export class ExtrasComponent implements OnInit {
  parentMessage = "extras"
  private sub: any;
  bookingParams;
  error: string;
  extras: Extras;
  selectedExtras: SelectedExtra[];
  loading: boolean;
  response: string;
  constructor(private _indexService: IndexserviceService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.sub = this.route.queryParams.subscribe(params => {
      this.bookingParams = params; // (+) converts string 'id' to a number

    });
    this.selectedExtras = [];
    this.extrasInfo();

  }

  setExtras(selected, code: string, maxQty: string, minQty: string, blockMin: string, blockMax: string, blockName: string) {

    const i = this.selectedExtras.findIndex(_item => _item.name === selected.name);
    let extra = new SelectedExtra();
    extra.name = selected.name;
    extra.bookingKey = code;
    extra.qty = selected.viewModel;
    extra.maxQty = maxQty;
    extra.minQty = minQty;
    extra.blockName = blockName;
    extra.blockMax = blockMax;
    extra.blockMin = blockMin;
    if (i > -1) {
      if (Number(extra.qty) != 0)
        this.selectedExtras[i] = extra;
      else
        this.selectedExtras.splice(i, 1);
    }
    else {
      this.selectedExtras.push(extra);
    }


  }

  selectExtras(form: any) {
    let rules: ValidationMessage[];
    rules = []
    let valid: boolean = true;
    let reqBlock: Block[];
    reqBlock = [];
    this.extras.blocks.forEach(element => {
      if (Number(element.blockMin) > 0) {
        let b = new Block();
        b.header = element.header;
        b.blockMin = element.blockMin;
        reqBlock.push(b);

      }
    })




    const grouped = this.selectedExtras.reduce((groups, selected) => {
      const groupName = selected.blockName;
      groups[groupName] = groups[groupName] || [];
      groups[groupName].push(selected);
      return groups;
    }, {});
    var result: any = Object.keys(grouped).map(key => ({ key, extras: grouped[key] }));
    let temp = []

    for (var b = 0; b < reqBlock.length; b++) {

      temp = result.filter(f => f.key == reqBlock[b].header)
      if (temp.length == 0) {
        let rule = new ValidationMessage();
        rule.valid = false;
        rule.msg = 'Too few tickets selected (0)' + ' in ' + reqBlock[b].header + ". The Minimum requirement is " + reqBlock[b].blockMin
        rules.push(rule)
      }
    }
    if (rules.length > 0) {
      BootstrapDialog.alert(rules[0].msg);
      return false;
    }


    for (var k = 0; k < result.length; k++) {


      var totalQty = 0;
      for (var j = 0; j < result[k].extras.length; j++) {
        totalQty += Number(result[k].extras[j].qty);
      }
      if (totalQty < Number(result[k].extras[0].blockMin)) {

        BootstrapDialog.alert('Too few tickets selected (' + totalQty + ')' + ' in ' + result[k].key + ". The Minimum requirement is " + result[k].extras[0].blockMin);
        return false;
      }
      if (totalQty > Number(result[k].extras[0].blockMax)) {

        BootstrapDialog.alert('Too many tickets selected (' + totalQty + ')' + ' in ' + result[k].key + ". The Maximum is " + result[k].extras[0].blockMax);
        return false;
      }
    }


    if (form.valid) {

      this.redirectToPersonal();

    }

  }

  redirectToPersonal() {
    let params: any = {}
    this.selectedExtras.forEach(element => {

      params[element.name] = element.qty + "*" + element.bookingKey;
    });
    params.reservationId = this.bookingParams.reservationId;
    params.tourSearchId = this.bookingParams.tourSearchId;
    params.tourDepartureId = this.bookingParams.tourDepartureId;
    params.hotelCode = this.bookingParams.hotelCode;
    params.hotelName = this.bookingParams.hotelName;
    params.tourCode = this.bookingParams.tourCode;
    params.accType = this.bookingParams.accType;
    params.tourTransport = this.bookingParams.tourTransport;
    params.NPC = this.bookingParams.NPC;
    params.deeplink = this.bookingParams.deeplink;
    params.tourCode = this.bookingParams.tourCode;
    params.tourDepDate = this.bookingParams.tourDepDate;
    params.depPointName = this.bookingParams.depPointName;
    params.fAirport = this.bookingParams.fAirport;
    params.fStation = this.bookingParams.fStation;
    params.fAdults = this.bookingParams.fAdults
    params.fChildren = this.bookingParams.fChildren
    params.fInfants = this.bookingParams.fInfants
    params.tourName = this.bookingParams.tourName
    var keyNames = Object.keys(this.bookingParams);

    keyNames.forEach(element => {
      if (element.startsWith("AccomCatsSel1_")) {
        params[element] = this.bookingParams[element];
      }
    });

    // Navigate to the personal page
    this.router.navigate(['/personal'], { queryParams: params });
  }
  extrasInfo() {
    this.loading = true;
    this._indexService.extrasInfo(this.bookingParams).subscribe(

      data => {
        this.loading = false;
        this.response = data;
        /*to convert xml text to javascript object*/
        let convert = require('xml-js');
        let result = convert.xml2js(data, { compact: true, spaces: 4 });

        if (!result.root.error) {

          let extra = new Extras();

          extra.tourTransport = result.root.bookvars.tourTransport._cdata;
          extra.hotelName = result.root.bookvars.hotelName._cdata;
          extra.accType = result.root.bookvars.accType._cdata;
          extra.tourCode = result.root.bookvars.tourCode._cdata;
          if (result.root.bookvars.depPointName)
            extra.depPointName = result.root.bookvars.depPointName._cdata;
          extra.tourDepDate = Utils.format(result.root.bookvars.tourDepDate._cdata);
          extra.deeplink = result.root.bookvars.deeplink._cdata;
          extra.hotelCode = result.root.bookvars.hotelCode._cdata;

          extra.fAdults = result.root.bookvars.fAdults._cdata;
          extra.fChildren = result.root.bookvars.fChildren._cdata;
          extra.tourName = result.root.bookvars.tourName._cdata;
          extra.blocks = [];
          if (!$.isEmptyObject(result.root.xResponse.xBlock)) {
            let blocks = $.makeArray(result.root.xResponse.xBlock);
            blocks.forEach(element => {
              let blockItem = new Block();
              blockItem.header = element.xBlockHeader._cdata
              blockItem.message = element.xBlockMsg._text;
              blockItem.body = element.xBlockBody._cdata;
              blockItem.blockMin = element.xBlockMin._text;
              blockItem.blockMax = element.xBlockMax._text
              blockItem.optionals = [];
              let optionals = $.makeArray(element.xItem);
              optionals.forEach(element => {

                let item = new OptionalItem();
                if (element.xTitleText)
                  item.title = element.xTitleText._cdata;
                else
                  item.title = element.xDesc._cdata;
                item.minQty = element.xSelectionMinQty._text;
                item.maxQty = element.xSelectionMaxQty._text;
                item.bookingDate = element.xBookingDate._cdata;
                item.rate = element.xPrice._text;
                item.xBookingKey = element.xBookingKey._cdata;
                item.priceBasis = element.xPrice._attributes.priceBasis

                if (element.xTktAvail)
                  item.tktAvail = element.xTktAvail._text;
                item.range = Array.apply(null, { length: Number(item.maxQty) + 1 }).map(Number.call, Number).slice(Number(item.minQty));
                let bodyText: string = !$.isEmptyObject(element.xBodyText) ? element.xBodyText._cdata : '';
                item.text = '<p><strong>' + item.title + '</strong></p><p><strong>' + item.bookingDate + '</strong></p>' + bodyText

                blockItem.optionals.push(item);
              });
              extra.blocks.push(blockItem)
            });

          }

          this.extras = extra;


        }
        else {
          this.error = result.root.error._text;
          console.error(result.root.error._text)
        }
      },
      err => console.error(err),
      () => console.log('done loading room')
    );
  }
}