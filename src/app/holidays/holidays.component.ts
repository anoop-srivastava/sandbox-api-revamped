import { Component, OnInit } from '@angular/core';
import {IndexserviceService} from '../indexservice.service';
import { Product } from '../dto/product';
import { Genre } from '../dto/product';
import * as $ from 'jquery';
import { Image } from '../dto/tour';
import { appSettings } from '../../assets/js/appSettings';
declare var require: any
@Component({
  selector: 'app-holidays',
  templateUrl: './holidays.component.html',
  styleUrls: ['./holidays.component.css']
})
export class HolidaysComponent implements OnInit {
  genres: Array<Genre> = [];
 response:string;
 loading:boolean;
 showProductImage:boolean
  constructor(private _indexService: IndexserviceService) { }

  ngOnInit() {
    this.showProductImage=appSettings.showProductImage
   
    this.getTours();
   
  }
  getTours() {
    this.loading=true;
    this._indexService.getTours().subscribe(
       data => {
        this.loading=false;
         this.response=data;
        /*to convert xml text to javascript object*/
        let convert = require('xml-js');
        let result  = convert.xml2js(data, {compact: true, spaces: 4});      
        
        result.root.segment.forEach((segment)=> {   
         
          if(segment.segmentname._text === 'Air Holidays & Breaks'){  
            let genres  =$.makeArray( segment.genre ); 
            genres.forEach((category) =>{  
              
            let genre= new Genre();
            genre.genrename=category.genrename._cdata.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">");; 
            
            genre.products=[];   
            let productArray  =$.makeArray(  category.products.product );    
            productArray.forEach((item) =>{              
               let product= new Product();                                  
               product.productname=item.productname._cdata.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">");;               
               product.availDates=item.productname._attributes.availDates;               
               product.dlc=item._attributes.dlc;
               if( this.showProductImage)
               {
               this._indexService.tourInfo(product.dlc).subscribe(
                data => {
                  let xml = require('xml-js');
                  let json  = convert.xml2js(data, {compact: true, spaces: 4}); 
                 if(json.root.content)
                 {
                  var pictures =$.makeArray(  json.root.content.pictures.picture );  
                  pictures.forEach((item) =>{
                   
                     if(item._attributes.imgType =='1000x440')  
                     {                          
                     product.image=item.url._cdata    
                     }          
                 
                    
                  })  
                }
                })   
              }             
               genre.products.push(product);  
                  
            })
          
       
            this.genres.push(genre);         
          }) 
          }
        
      }) 
           
        },
       err => console.error(err),
       () => console.log('done loading tours'  )
     );
   }

}
