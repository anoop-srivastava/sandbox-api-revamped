import { TestBed } from '@angular/core/testing';

import { IndexserviceService } from './indexservice.service';

describe('IndexserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IndexserviceService = TestBed.get(IndexserviceService);
    expect(service).toBeTruthy();
  });
});
