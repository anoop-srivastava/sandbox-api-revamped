import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';


// const httpOptions = {
//   headers: new HttpHeaders({ 'Accept': 'application/xml' })
// };

@Injectable({
  providedIn: 'root'
})
export class IndexserviceService {
  baseUrl:string = "https://api.newmarket.travel";

  constructor(private http:HttpClient) { }
  getTours() {
   
    return this.http.get(this.baseUrl + '/xIndex2.asp?pswd=NMP12780&type=x', {responseType: 'text'});
 }
 tourInfo(uid:string){ 
    return this.http.get(this.baseUrl + '/xData2.asp?uid=' + uid, {responseType: 'text'});
}

accoInfo(params){ 
  
  return this.http.get(this.baseUrl + '/xBooking/Svr2/xSelHotelServer.asp', {params: params,responseType: 'text'});
}
roomInfo(params){ 
  
  return this.http.get(this.baseUrl + '/xBooking/Svr2/xSelRoomServer.asp', {params: params,responseType: 'text'});
}
extrasInfo(params){ 
  
  return this.http.get(this.baseUrl + '/xBooking/Svr2/xSelXtraServer.asp', {params: params,responseType: 'text'});
}
personalInfo(params){ 
  
  return this.http.get(this.baseUrl + '/xBooking/Svr2/xSelPaxServerPlusV2.asp', {params: params,responseType: 'text'});
}
confirmInfo(params){ 
  
  return this.http.get(this.baseUrl + '/xBooking/Svr2/xPaxAndConfirmServer.asp', {params: params,responseType: 'text'});
}
postToPaymentGateway(params){ 
  
  return this.http.get(this.baseUrl + '/xBooking/Svr2/xPaymentGateway.asp', {params: params,responseType: 'text'});
}
completeInfo(params){ 
  
  return this.http.get(this.baseUrl + '/xBooking/Svr2/xThankYouServer.asp', {params: params,responseType: 'text'});
}
}
