import { Component, OnInit } from '@angular/core';
import { IndexserviceService } from '../indexservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery';
declare var require: any
declare var BootstrapDialog: any
import { Personal, PaxInfo, MarketingPreferences, Passenger, Room, Extra, Option } from '../dto/personal';
import Utils from '../utils'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { IMyDpOptions } from 'mydatepicker';
import { race } from 'rxjs';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit {
  parentMessage = "personal"
  private sub: any;
  bookingParams;
  error: string;
  private personal: Personal;
  checkedList: MarketingPreferences[];
  loading: boolean;
  response: string;
  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd'
  };

  // Initialized to specific date (09.10.2018).
  //public model: any = { date: { year: 2018, month: 10, day: 9 } };

  constructor(private _indexService: IndexserviceService, private route: ActivatedRoute, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.sub = this.route.queryParams.subscribe(params => {
      this.bookingParams = params; // (+) converts string 'id' to a number

    });
    this.checkedList = []

    this.personalInfo();
  }

  onCheckboxChange(option, event, index) {

    if (event.target.checked) {

      let pref = new MarketingPreferences();
      pref.id = option.id;
      pref.index = index;
      this.checkedList.push(pref);
    } else {
      for (var i = 0; i < this.checkedList.length; i++) {
        if (this.checkedList[i].id == option.id) {
          this.checkedList.splice(i, 1);

        }
      }
    }

  }
  submitPersonal(f: NgForm) {
    if (f.valid) {

      let params: any = {}

      params.reservationId = this.bookingParams.reservationId;
      params.tourSearchId = this.bookingParams.tourSearchId;
      params.tourDepartureId = this.bookingParams.tourDepartureId;
      params.hotelCode = this.bookingParams.hotelCode;
      params.hotelName = this.bookingParams.hotelName;
      params.tourCode = this.bookingParams.tourCode;
      params.accType = this.bookingParams.accType;
      params.tourTransport = this.bookingParams.tourTransport;
      params.NPC = this.bookingParams.NPC;
      params.deeplink = this.bookingParams.deeplink;
      params.tourCode = this.bookingParams.tourCode;
      params.tourDepDate = this.bookingParams.tourDepDate;
      params.depPointName = this.bookingParams.depPointName;
      params.fAirport = this.bookingParams.fAirport;
      params.fStation = this.bookingParams.fStation;
      params.fAdults = this.bookingParams.fAdults
      params.fChildren = this.bookingParams.fChildren
      params.fInfants = this.bookingParams.fInfants
      params.tourName = this.bookingParams.tourName
      var keyNames = Object.keys(this.bookingParams);

      keyNames.forEach(element => {
        if (element.startsWith("AccomCatsSel1_")) {
          params[element] = this.bookingParams[element];
        }
        if (element.startsWith("ExtrasSel_")) {
          params[element] = this.bookingParams[element];
        }
      });

      let pax = [];
      for (let i = 1; i <= this.personal.paxDetails.passengers.length; i++) {

        params["Paxtype_" + i] = this.personal.paxDetails.passengers[i - 1].paxType
        params["id_" + i] = this.personal.paxDetails.passengers[i - 1].paxId
        params["title_" + i] = f.value["title_" + i]
        params["initials_" + i] = f.value["initials_" + i]
        params["surname_" + i] = f.value["surname_" + i]
        params["dob_" + i] = f.value["dob_" + i].formatted
        debugger
        params["room_" + i] = f.value["room_" + i]["roomId"]

        let temp = this.personal.paxDetails.passengers[i - 1].rooms.filter(r => r.roomSelection == f.value["room_" + i]["roomSelection"])
        pax[i] = { 'maxOcc': temp[0].maxOcc, 'roomId': temp[0].roomId, 'room': temp[0].roomSelection };
        params["gender_" + i] = f.value["gender_" + i]
        params["PAXExtrasSel_" + i + "_1"] = f.value["PAXExtrasSel_" + i + "_1"]
        params["InsSel_1_" + i] = f.value["InsSel_1_" + i]
        params["InsSel_2_" + i] = f.value["InsSel_2_" + i]
        params["policy_" + i] = f.value["policy_" + i]
        params["polnumber_" + i] = f.value["polnumber_" + i]
        params["polphone_" + i] = f.value["polphone_" + i]

      }

      const grouped = pax.reduce((groups, selected) => {
        const groupName = selected.room;

        groups[groupName] = groups[groupName] || [];
        groups[groupName].push(selected);

        return groups;
      }, {});
      var result: any = Object.keys(grouped).map(key => ({ key, rooms: grouped[key] }));
      
      for (let k = 0; k < result.length; k++) {
        if (result[k].rooms.length > result[k].rooms[0].maxOcc) {
          BootstrapDialog.alert('Too many people in ' + result[k].rooms[0].room);
          return false;
        }
      }

      params["CCardAddress1"] = f.value["CCardAddress1"]
      params["CCardAddress2"] = f.value["CCardAddress2"]
      params["CCardCity"] = f.value["CCardCity"]
      params["CCardCountry"] = f.value["CCardCountry"]
      params["CCardPostCode"] = f.value["CCardPostCode"]
      params["MainTelWork"] = f.value["MainTelWork"]
      params["MainTelHome"] = f.value["MainTelHome"]
      params["MainEmail"] = f.value.passwords["MainEmail"]
      params["MainEmailChq"] = f.value.passwords["MainEmailChq"]
      params["PromoSel"] = f.value["PromoSel"]


      for (let j = 0; j < this.checkedList.length; j++) {
        params["MarketingPrefs_" + this.checkedList[j].index] = this.checkedList[j].id

      }


      // Navigate to the extras page
      this.router.navigate(['/confirm'], { queryParams: params });
    }
  }



  personalInfo() {
    this.loading = true;
    this._indexService.personalInfo(this.bookingParams).subscribe(

      data => {
        this.loading = false;
        this.response = data;
        /*to convert xml text to javascript object*/
        let convert = require('xml-js');
        let result = convert.xml2js(data, { compact: true, spaces: 4 });

        if (!result.root.error) {

          let personal = new Personal();

          personal.tourTransport = result.root.bookvars.tourTransport._cdata;
          personal.hotelName = result.root.bookvars.hotelName._cdata;
          personal.accType = result.root.bookvars.accType._cdata;
          personal.tourCode = result.root.bookvars.tourCode._cdata;
          if (result.root.bookvars.depPointName)
            personal.depPointName = result.root.bookvars.depPointName._cdata;
          personal.tourDepDate = Utils.format(result.root.bookvars.tourDepDate._cdata);
          personal.deeplink = result.root.bookvars.deeplink._cdata;
          personal.hotelCode = result.root.bookvars.hotelCode._cdata;

          personal.fAdults = result.root.bookvars.fAdults._cdata;
          personal.fChildren = result.root.bookvars.fChildren._cdata;
          personal.tourName = result.root.bookvars.tourName._cdata;
          personal.paxNote = result.root.xResponse.paxInfo.paxnamesNote._cdata
          let preferences = $.makeArray(result.root.xResponse.marketingPreferences.pref);
          personal.marketingPreferences = [];
          preferences.forEach(element => {
            let pref = new MarketingPreferences();
            pref.id = element.ResponseID._attributes.id
            pref.value = element.ResponseID._cdata
            personal.marketingPreferences.push(pref)
          });
          //xResponse.paxInfo.passenger[""0""].roomSelection.rooms.room._cdata

          let paxInfo = new PaxInfo();
          paxInfo.passengers = []
          let passengers = $.makeArray(result.root.xResponse.paxInfo.passenger);

          passengers.forEach(element => {
            let passenger = new Passenger()
            passenger.paxId = element.paxId._text;
            passenger.paxType = element.paxType._text;
            let extras = $.makeArray(element.extras.extra);
            passenger.extras = []
            extras.forEach(element => {
              let extra = new Extra()
              extra.group = element._attributes.group;
              extra.maxQty = element._attributes.maxQty;
              extra.minQty = element._attributes.minQty;
              extra.options = [];
              let options = $.makeArray(element.ResponseID);
              options.forEach(element => {
                let option = new Option()
                option.responseId = element._attributes.id;
                option.text = element._cdata;
                extra.options.push(option)
              });
              passenger.extras.push(extra)
            });
            let rooms = $.makeArray(element.roomSelection.rooms.room);
            passenger.rooms = []

            rooms.forEach(element => {
              let room = new Room();
              room.roomSelection = element._cdata
              let paxrooms = $.makeArray(result.root.xResponse.paxInfo.roomSelectionControls.paxroom);
              paxrooms.forEach(j => {
                if (j.descr._cdata == room.roomSelection) {
                  room.roomId = j._attributes.id
                  room.minOcc = j.minOcc._text
                  room.maxOcc = j.maxOcc._text

                }
              })

              passenger.rooms.push(room)

            });

            paxInfo.passengers.push(passenger)
          });
          // paxInfo.paxCount =Array(Number(result.root.xResponse.paxInfo.paxCount._text)).fill(1);
          paxInfo.paxTitles = [];
          let titleArray = $.makeArray(result.root.xResponse.paxInfo.paxTitles.paxTitle);
          titleArray.forEach(element => {
            paxInfo.paxTitles.push(element._text)
          });

          personal.paxDetails = paxInfo;

          this.personal = personal
          debugger
        }
        else {
          this.error = result.root.error._text;
          console.error(result.root.error._text)
        }
      },
      err => console.error(err),
      () => console.log('done loading personal')
    );
  }
}
