import { Component, OnInit } from '@angular/core';
import {IndexserviceService} from '../indexservice.service';
import { ActivatedRoute, Router}  from '@angular/router';
import { Tour } from '../dto/tour';
import { Image } from '../dto/tour';
import { TourData } from '../dto/tour';
import { BookingParams } from '../dto/tour';
// import {BookingserviceService} from '../bookingservice.service';
import {NgForm} from '@angular/forms';
import Utils from '../utils'
declare var require: any
declare var BootstrapDialog: any
import * as $ from 'jquery';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit{
 
  private sub: any;
 
  depdates:any;
  uid: string;
  loading:boolean
  private tour:Tour;
  private error:string;
  response:string;
  constructor(private _indexService: IndexserviceService,private route: ActivatedRoute,private router:Router) { }

  ngOnInit() {
   
    this.sub = this.route.queryParams.subscribe(params => {
      this.uid = params['uid']; // (+) converts string 'id' to a number
    //  this.parentMessage=this.uid.substring(3)
   });
   this.tourInfo();
   
  }
  onChange(name) {
    
    this.depdates = this.tour.tourData.filter((item)=> item.name == name)[0].dates;
    }
    submitBooking(f: NgForm)
    {
     
      if (f.valid) {
      let params=new BookingParams();
      
      if(Number(f.value.children) > 0 && this.tour.genreCode!="AIRDAYT")
      {
        BootstrapDialog.alert('Please call our Customer Services Department for bookings with Children.');
        return false;
      }

      if( Number(f.value.infant) > 0 && this.tour.genreCode!="AIRDAYT")
      {
        BootstrapDialog.alert('Please call our Customer Services Department for bookings with Infants.');
        return false;
      }
      
      params.tourTransport= this.tour.transportType;
      params.NPC= this.tour.deeplink.substring(0, 3);;
      params.deeplink= this.tour.deeplink;
      params.tourCode= this.tour.tourCode;
      params.tourDepDate=Utils.formatDate(f.value.date);
      params.fAdults= f.value.adult;
      params.fChildren= f.value.children;
      params.fInfants= f.value.infant;      
      params.tourName=this.tour.prodName;
     
     
      switch(params.tourTransport) {
        case "AIR":
        params.depPointName= f.value.airport.name;
        params.fAirport= f.value.airport.code;
        break;
        case "RAIL":
        params.depPointName= f.value.airport.name;
        params.fStation= f.value.airport.code;
          break;
          case "MYOW":
          // code block
      }
      if(f.value["touronly"] ==true)
      params.touronly='Y';
      if(f.value["agentCode"]==true)      
      {
      params.agentCode= 'TEST2';
      params.agentContactEmail="test.agent@newmarketholidays.co.uk";
      params.agentPassword="bedbugs"
      params.agentReference="Mr Test Agent"
      }

      this.router.navigate(['/accommodations'],{ queryParams:  params });
      
    }
  }
     
  tourInfo() {
    this.loading=true;
    this._indexService.tourInfo(this.uid).subscribe(
       data => {
        /*to convert xml text to javascript object*/
        this.loading=false;
        this.response=data;
        let convert = require('xml-js');
        let result  = convert.xml2js(data, {compact: true, spaces: 4});      
        let tour= new Tour();
       
        /*Get main Classification details */
        if(!result.root.error){   
         
        tour.transportType=result.root.content.Info.transportType._text;
        tour.segment=result.root.content.Info.segment._text;
        tour.genre=result.root.content.Info.genre._text;
        tour.prodcat=result.root.content.Info.productName._text;
        tour.holtype=result.root.content.Info.productName._text;
        tour.prodName=result.root.content.Info.productName._text;
        tour.prodWebName=result.root.content.Info.newmarketWebname._text;
        tour.deeplink=result.root.content.Info.navigationID._text;
        tour.dateStamp=result.root.content.Info.dateStamp._text;
        tour.tourCode=result.root.content.Info.baseTourcode4content._text;   
        tour.genreCode=result.root.content.Info.genre._attributes.gCode
        
        tour.productCode=tour.deeplink.substring(3);
        /* Fetch the price data for the product*/     
        
        tour.bookOnLine=result.root.content.Info.bol._text;
        tour.duration=result.root.content.Info.strapline.overNightDuration._text;
        tour.period=result.root.content.Info.strapline.period._text;
        tour.fromPrice=result.root.content.Info.strapline.pricing.fromPrice._text;
        tour.fromPriceDate=result.root.content.Info.strapline.pricing.fromPriceDate._text;
        tour.images=[];
        
        if(Array.isArray(result.root.content.pictures.picture))
        {
          result.root.content.pictures.picture.forEach((item) =>{
           
            let image= new Image();   
                                         
            image.url=item.url._cdata              
            image.type=item._attributes.imgType;    
            image.slot=item._attributes.slot;     
            tour.images.push(image);  
          })  
          
        }
        else{
          let image= new Image();   
            if(result.root.content.pictures.picture)     
            {    
                        
          image.url=result.root.content.pictures.picture.url._cdata    
          image.slot=result.root.content.pictures.picture._attributes.slot;           
          image.type=result.root.content.pictures.picture._attributes.imgType;     
          tour.images.push(image);    
            }
        }
 

        /*Process the Content data for the product */ 
        if(result.root.content.copyText.tourIntro && result.root.content.copyText.tourIntro.textblock){
        var tourIntros =$.makeArray( result.root.content.copyText.tourIntro.textblock.title ); 
        tour.introTitles=[];
        tourIntros.forEach((element) =>{
          if(element) 
          tour.introTitles.push(element._cdata)
       })

       var tourBodies =$.makeArray( result.root.content.copyText.tourIntro.textblock.body);
       tour.introBodies=[];
       tourBodies.forEach((element) =>{
       
        if(element) 
        tour.introBodies.push(element.text._cdata)
     })
    }
        if(result.root.content.copyText.intro && result.root.content.copyText.intro.textblock && Array.isArray(result.root.content.copyText.intro.textblock.body))
        {
        result.root.content.copyText.intro.textblock.body.forEach((item) =>{
          if (item.text)   
          tour.intro+=item.text._cdata
       })
        }
        else if (result.root.content.copyText.intro && result.root.content.copyText.intro.textblock){
          tour.intro=result.root.content.copyText.intro.textblock.body.text._cdata;
        }
        if(result.root.content.copyText.intro && result.root.content.copyText.intro.textblock)
        tour.tourIntroTitle=result.root.content.copyText.intro.textblock.title._cdata;
        if(result.root.content.copyText.wpi && result.root.content.copyText.wpi.textblock && Array.isArray(result.root.content.copyText.wpi.textblock.body))
        {
          tour.wpi=result.root.content.copyText.wpi.textblock.body[0].text._cdata.split("<br/>");
        
        }
        else if (result.root.content.copyText.wpi && result.root.content.copyText.wpi.textblock){
         
          tour.wpi=result.root.content.copyText.wpi.textblock.body.text._cdata.split("<br/>");
        }
        if(result.root.content.copyText.wpi && result.root.content.copyText.wpi.textblock && Array.isArray(result.root.content.copyText.wpi.textblock.title))
        {
        tour.wpiTitle=result.root.content.copyText.wpi.textblock.title[0]._cdata; 
        }
        else if (result.root.content.copyText.wpi && result.root.content.copyText.wpi.textblock){
          tour.wpiTitle=result.root.content.copyText.wpi.textblock.title._cdata;  
        }
       
            if(result.root.content.copyText.itinerary && result.root.content.copyText.itinerary.textblock)      {    
              tour.ItinBody=[];  
        result.root.content.copyText.itinerary.textblock.body.forEach((item) =>{              
          if (item.text)                             
          tour.ItinBody.push(item.text._cdata);
       })
      }
       
       if(result.root.content.copyText.itinerary && result.root.content.copyText.itinerary.textblock)      {  
        tour.ItinTitle=[];                
        result.root.content.copyText.itinerary.textblock.title.forEach((item) =>{              
          if (item._cdata)                             
          tour.ItinTitle.push(item._cdata);
       })
      }
       
      
       if(result.root.content.copyText.furtherInfo && result.root.content.copyText.furtherInfo.textblock)      {    
        tour.furtherInfoBody=[];    
        result.root.content.copyText.furtherInfo.textblock.body.forEach((item) =>{              
          if (item.text)                             
          tour.furtherInfoBody.push(item.text._cdata);
       })
      }
      
       if(result.root.content.copyText.furtherInfo && result.root.content.copyText.furtherInfo.textblock)      {   
        tour.furtherInfoTitle=[];     
       result.root.content.copyText.furtherInfo.textblock.title.forEach((item) =>{              
        if (item._cdata)                             
        tour.furtherInfoTitle.push(item._cdata);
     })
    }
    tour.tourData=[];
    /*Process the booking widget data */ 
    switch(tour.transportType) {
      case "AIR":
      
      if(Array.isArray(result.root.tourData.air.airports.airport))
      {
      result.root.tourData.air.airports.airport.forEach((item) =>{              
        let tourData= new TourData();
        tourData.name=item.name._text;
        tourData.code=item._attributes.iata
        tourData.dates=[];
        if(Array.isArray(item.date))
        {
        item.date.forEach((item) =>{    
                    
          tourData.dates.push(item._text);
       })
      }
      else{
        tourData.dates.push(item.date._text);
      }
      tour.tourData.push(tourData);
     })
     
    
    }
    else{
      let tourData= new TourData();
      tourData.name=result.root.tourData.air.airports.airport.name._text;
      tourData.code=result.root.tourData.air.airports.airport._attributes.stn;
      tourData.dates=[];
      if(Array.isArray(result.root.tourData.air.airports.airport.date)){
      result.root.tourData.air.airports.airport.date.forEach((item) =>{    
                  
        tourData.dates.push(item._text);
     })
    }
    else{
      tourData.dates.push(result.root.tourData.air.airports.airport.date._text);
    }
    tour.tourData.push(tourData);
    }
    
        break;
      case "RAIL":



  if(Array.isArray(result.root.tourData.rail.stations.station))
  {
  result.root.tourData.rail.stations.station.forEach((item) =>{              
    let tourData= new TourData();
    tourData.name=item.name._text;
    
    tourData.code=item._attributes.stn
    tourData.dates=[];
    if(Array.isArray(item.date))
    {
    item.date.forEach((item) =>{    
                
      tourData.dates.push(item._text);
   })
  }
  else{
    tourData.dates.push(item.date._text);
  }
  tour.tourData.push(tourData);
 })
 

}
else{
  
  let tourData= new TourData();
  tourData.name=result.root.tourData.rail.stations.station.name._text;
  tourData.code=result.root.tourData.rail.stations.station._attributes.stn
  tourData.dates=[];
  if(Array.isArray(result.root.tourData.rail.stations.station.date)){
  result.root.tourData.rail.stations.station.date.forEach((item) =>{    
              
    tourData.dates.push(item._text);
 })
}
else{
  tourData.dates.push(result.root.tourData.rail.stations.station.date._text);
}
tour.tourData.push(tourData);
}
        break;
      case "MYOW":
        // code block
        
        if(Array.isArray(result.root.tourData.myow.dates)){
          result.root.tourData.myow.dates.date.forEach((item) =>{  
                      
            let tourData= new TourData();
            tourData.dates=[];
            tourData.dates.push(item.date._text)
            tour.tourData.push(tourData)
         })
         
        }
        else{
          // let tourData= new TourData();
          // tourData.dates=[];
          tour.tourData.push(result.root.tourData.myow.dates.date._text);
        
        }
       
    }
        this.tour=tour;
        
    }
    else{
      
      this.error=result.root.error._text;
      console.error(result.root.error._text)
    }
        },
       err => console.error(err),
       () => console.log('done loading tour'  )
     );
   }

}
