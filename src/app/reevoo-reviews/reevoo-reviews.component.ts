import { Component, OnInit,Input  } from '@angular/core';

@Component({
  selector: 'app-reevoo-reviews',
  template: `<reevoo-reviewable-badge trkref="NMH" sku="{{ childMessage }}"></reevoo-reviewable-badge>`,
  styleUrls: ['./reevoo-reviews.component.css']
})
export class ReevooReviewsComponent implements OnInit {
  @Input() childMessage: string;
  constructor() { }

  ngOnInit() {
  }

}
