import { Component, OnInit } from '@angular/core';
import { IndexserviceService } from '../indexservice.service';

import { ActivatedRoute, Router } from '@angular/router';
import { BookingParams } from '../dto/tour';
import { Room, RoomingData, SelectedAccoUnit } from '../dto/room';
import * as $ from 'jquery';
import Utils from '../utils'
import { WorkflowService } from '../workflow/workflow.service';
declare var require: any
import { STEPS } from '../workflow/workflow.model';
declare var BootstrapDialog: any
@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit {
  parentMessage = "rooms"
  bookingParams;
  private room: Room;
  error: string;
  private sub: any;
  loading: boolean;
  selectedAccoUnits: SelectedAccoUnit[];
  response:string;
  constructor(private _indexService: IndexserviceService, private workflowService: WorkflowService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.sub = this.route.queryParams.subscribe(params => {
      this.bookingParams = params; // (+) converts string 'id' to a number

    });
    this.selectedAccoUnits = [];
    this.roomInfo();
  }
  setRoomClone(selected, code: string, maxOcc: string, minOcc: string) {
    const i = this.selectedAccoUnits.findIndex(_item => _item.name === selected.name);
    let accoUnit = new SelectedAccoUnit();
    accoUnit.name = selected.name;
    accoUnit.code = code;
    accoUnit.qty = selected.viewModel;
    accoUnit.maxOcc = maxOcc;
    accoUnit.minOcc = minOcc;
    if (i > -1) {
      if(Number(accoUnit.qty)!=0)
      this.selectedAccoUnits[i] = accoUnit;
      else
      this.selectedAccoUnits.splice(i, 1);
    }
    else {
      this.selectedAccoUnits.push(accoUnit);
    }


  }

  selectRoom(form: any) {
    if (form.valid) {
      this.workflowService.validateStep(STEPS.rooms);
      let maxOccupancy: number = 0;
      let minOccupancy: number = 0;
      let numRooms: number = 0;

      let params: any = {}

      if (!this.selectedAccoUnits) {
        BootstrapDialog.alert('You have not selected enough rooms/cabins (' + numRooms + '). Please try again.');
        return false;
      }
      this.selectedAccoUnits.forEach(element => {
        
        numRooms += Number(element.qty)
        maxOccupancy += Number(element.maxOcc) * Number(element.qty);
        minOccupancy += Number(element.minOcc) * Number(element.qty);
        params[element.name] = element.qty + "*" + element.code;
      });

      let partySize = Number(this.bookingParams.fAdults) + Number(this.bookingParams.fChildren);
      if (partySize > maxOccupancy) {
        BootstrapDialog.alert('You have not selected enough rooms/cabins (' + numRooms + '). Please try again.');
        return false;
      }
      if (partySize < minOccupancy) {
        BootstrapDialog.alert('You have selected too many rooms/cabins (' + numRooms + '). Please try again.');
        return false;
      }
      params.reservationId = this.bookingParams.reservationId;
      params.tourSearchId = this.bookingParams.tourSearchId;
      params.tourDepartureId = this.bookingParams.tourDepartureId;
      params.hotelCode = this.bookingParams.hotelCode;
      params.hotelName = this.bookingParams.hotelName;
      params.tourCode = this.bookingParams.tourCode;
      params.accType = this.bookingParams.accType;
      params.tourTransport = this.bookingParams.tourTransport;
      params.NPC = this.bookingParams.NPC;
      params.deeplink = this.bookingParams.deeplink;
      params.tourCode = this.bookingParams.tourCode;
      params.tourDepDate = this.bookingParams.tourDepDate;
      params.depPointName = this.bookingParams.depPointName;
      params.fAirport = this.bookingParams.fAirport;
      params.fStation = this.bookingParams.fStation;
      params.fAdults = this.bookingParams.fAdults
      params.fChildren = this.bookingParams.fChildren
      params.fInfants = this.bookingParams.fInfants
      params.tourName = this.bookingParams.tourName


      // Navigate to the extras page
      this.router.navigate(['/extras'], { queryParams: params });
    }
  }
  roomInfo() {
    this.loading = true;
    this._indexService.roomInfo(this.bookingParams).subscribe(

      data => {
        this.loading = false;
        this.response=data;
        /*to convert xml text to javascript object*/
        let convert = require('xml-js');
        let result = convert.xml2js(data, { compact: true, spaces: 4 });

        if (!result.root.error) {
          let room = new Room();

          room.tourTransport = result.root.bookvars.tourTransport._cdata;
          room.hotelName = result.root.bookvars.hotelName._cdata;
          room.accType = result.root.bookvars.accType._cdata;
          room.tourCode = result.root.bookvars.tourCode._cdata;
          if (result.root.bookvars.depPointName)
            room.depPointName = result.root.bookvars.depPointName._cdata;
          room.tourDepDate = Utils.format(result.root.bookvars.tourDepDate._cdata);
          room.deeplink = result.root.bookvars.deeplink._cdata;
          room.hotelCode = result.root.bookvars.hotelCode._cdata;

          room.fAdults = result.root.bookvars.fAdults._cdata;
          room.fChildren = result.root.bookvars.fChildren._cdata;
          room.tourName = result.root.bookvars.tourName._cdata;
          room.availability = [];

          var roomingData = $.makeArray(result.root.xResponse.roomingData.room);
          roomingData.forEach(element => {
            let item = new RoomingData();

            item.code = element.code._text;
            item.descr = element.descr._cdata;
            item.maxOcc = element.maxOcc._text;
            item.minOcc = element.minOcc._text;
            item.maxQty = element.maxQty._text;
            item.minQty = element.minQty._text;
            item.range = Array.apply(null, { length: Number(item.maxQty) + 1 }).map(Number.call, Number).slice(Number(item.minQty));

            item.rate = element.rate._text;
            item.wasPrice = element.wasPrice._text;
            item.unitNumber = element.unitNumber._cdata;
            room.availability.push(item)
          });
          this.room = room;

        }
        else {
          this.error = result.root.error._text;
          console.error(result.root.error._text)
        }
      },
      err => console.error(err),
      () => console.log('done loading room')
    );
  }
}
