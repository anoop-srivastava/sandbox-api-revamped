export default class Utils {
    static formatDate(date):string {    
        var tourDepDate= new Date(date);
        var y = tourDepDate.getFullYear();
        var m = tourDepDate.getMonth() + 1;
        var d = tourDepDate.getDate();
        var mm = m < 10 ? '0' + m : m;
        var dd = d < 10 ? '0' + d : d;
        return '' + y + mm + dd;
    }
    static format(dateString) :string
        {
            var year        = dateString.substring(0,4);
            var month       = Number(dateString.substring(4,6));
            var day         = dateString.substring(6,8);

        
          
          let months = new Array();
          months[0] = "January";
          months[1] = "February";
          months[2] = "March";
          months[3] = "April";
          months[4] = "May";
          months[5] = "June";
          months[6] = "July";
          months[7] = "August";
          months[8] = "September";
          months[9] = "October";
          months[10] = "November";
          months[11] = "December";

        
          
          return    day  + " " +months[month -1] + " " + year;
          
            
         }
   
}