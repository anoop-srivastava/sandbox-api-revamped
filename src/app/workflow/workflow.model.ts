export const STEPS = {
    accommodations: 'accommodations',
    rooms: 'rooms',   
    extras: 'extras',
    personal: 'personal',
    confirm: 'confirm',
    complete: 'complete'

}
